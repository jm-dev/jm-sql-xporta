/*
 * Copyright (C) 2013 Michael
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.jm.jmsql.xporta.models;

import com.jm.commons.cryptography.Crypto;
import com.jm.jdbc.xporta.objects.Database;
import com.jm.jdbc.xporta.objects.Definition;
import com.jm.jmsql.xporta.utils.Settings;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Michael L.R. Marques
 */
public class DefinitionComboBoxModel implements ComboBoxModel {
    
    //
    private List<Definition> definitions;
    private int index;
    
    /**
     * 
     * @param database 
     */
    public DefinitionComboBoxModel(Database database) {
        this.definitions = new ArrayList();
        this.index = -1;
        // 
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        Document document = null;
        try {
            document = factory.newDocumentBuilder().parse(Settings.getDatabasesFile());
        } catch (ParserConfigurationException | XPathExpressionException | SAXException | IOException ex) {
            ex.printStackTrace();
        }
        try {
            NodeList nodeList = (NodeList) XPathFactory.newInstance().newXPath().evaluate("/databases/database[@name='" + database.getName() + "']/definitions/definition", document, XPathConstants.NODESET);
            for(int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeName().equals("definition") &&
                        node.hasAttributes()) {
                    NamedNodeMap map = node.getAttributes();
                    this.definitions.add(new Definition(database, map.getNamedItem("name").getNodeValue(), map.getNamedItem("datasource").getNodeValue(), map.getNamedItem("username").getNodeValue(), Crypto.decrypt(map.getNamedItem("password").getNodeValue())));
                }
            }
        } catch (XPathExpressionException xpee) {
            xpee.printStackTrace();
        }
    }
    
    /**
     * 
     * @param item 
     */
    @Override
    public void setSelectedItem(Object item) {
        if (!this.definitions.contains((Definition) item)) {
            this.index = this.definitions.indexOf(item);
        } else {
            this.index = 0;
        }
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Object getSelectedItem() {
        return this.definitions.get(this.index);
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public int getSize() {
        return this.definitions.size();
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public Object getElementAt(int index) {
        return this.definitions.get(index);
    }
    
    /**
     * 
     * @param l 
     */
    @Override
    public void addListDataListener(ListDataListener l) {}
    
    /**
     * 
     * @param l 
     */
    @Override
    public void removeListDataListener(ListDataListener l) {}
    
}
