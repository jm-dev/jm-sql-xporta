/*
 * Copyright (C) 2013 Michael
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.jm.jmsql.xporta.models;

import com.jm.jdbc.xporta.objects.Definition;
import com.jm.jdbc.xporta.objects.Schema;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;

/**
 *
 * @author Michael L.R. Marques
 */
public class SchemaComboBoxModel implements ComboBoxModel {
    
    //
    private List<Schema> schemas;
    private int index;
    
    /**
     * 
     * @param definition 
     */
    public SchemaComboBoxModel(Definition definition) {
        this.index = -1;
        try (Connection connection = definition.connect()) {
            DatabaseMetaData dmd = connection.getMetaData();
            try (ResultSet results = dmd.getSchemas()) {
                while (results.next()) {
                    try {
                        this.schemas.add(new Schema(definition, results.getString(1)));
                    } catch (SQLException sqle) {
                        sqle.printStackTrace();
                    }
                }
            }
        } catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * 
     * @param item
     */
    @Override
    public void setSelectedItem(Object item) {
         if (this.schemas.contains(item)) {
             this.index = this.schemas.indexOf(item);
         } else {
             this.index = 0;
         }
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Object getSelectedItem() {
        return this.schemas.get(index);
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public int getSize() {
        return this.schemas.size();
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public Object getElementAt(int index) {
        return this.schemas.get(index);
    }
    
    /**
     * 
     * @param l 
     */
    @Override
    public void addListDataListener(ListDataListener l) {}
    
    /**
     * 
     * @param l 
     */
    @Override
    public void removeListDataListener(ListDataListener l) {}
    
}
