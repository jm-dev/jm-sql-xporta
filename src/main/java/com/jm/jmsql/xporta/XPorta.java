/*
 * Copyright (C) 2013 Michael
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.jm.jmsql.xporta;

import com.jm.commons.components.JMFrame;
import com.jm.commons.components.panel.ImagePanel;
import com.jm.xporta.exceptions.XPortException;
import com.jm.xporta.plugin.XPort;
import com.jm.xporta.plugin.XPortResult;
import java.awt.Container;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JProgressBar;
import javax.swing.event.ChangeEvent;
import javax.swing.filechooser.FileFilter;

/**
 * 
 * @created Feb 12, 2013
 * @author Michael L.R. Marques
 */
public abstract class XPorta extends JMFrame implements XPort<ResultSet, File> {
    
    private JButton btnCancel;
    private JButton btnExport;
    private JCheckBox chbOpenOnComplete;
    private JProgressBar progress;
    protected ImagePanel panelBackground;
    
    private ResultSet results;
    private File output;
    private XPortResult result;

    protected File currentDirectory;

    /**
     * Creates new form XPorta
     */
    public XPorta() {
        super();
        initComponents();
        this.result = new XPortResult();
    }

    /**
     *
     * @param resultSet
     * @param params
     * @return XPortResult
     * @throws XPortException
     */
    @Override
    public XPortResult initialize(ResultSet resultSet, Object... params) throws XPortException {
        this.results = resultSet;
        try {
            if (resultSet != null) {
                this.results.beforeFirst();
            }
        } catch (SQLException sqle) {
            throw new XPortException(sqle);
        }
        if (params != null &&
                params.length > 0) {
            for (Object param : params) {
                if (param != null) {
                    if (param instanceof Rectangle) {
                        setBounds((Rectangle) param);
                    } else if (param instanceof Point) {
                        setLocation((Point) param);
                    } else if (param instanceof Container) {
                        setBoundsToCenterOfParent((Container) param);
                    } else if (param instanceof File) {
                        this.currentDirectory = (File) param;
                    } else if (param instanceof Image) {
                        setIconImage((Image) param);
                    }
                }
            }
        }

        setVisible(true);

        if (this.currentDirectory == null) {
            this.currentDirectory = new File("./");
        }

        return this.result;
    }

    /**
     *
     * @return
     */
    @Override
    public String getName() {
        return "Basic XPorta Plugin";
    }

    /**
     *
     * @return
     */
    @Override
    public String getDescription() {
        return "Basic plugin component that can be inherited, for ease of use.";
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public boolean isExcluded() {
        return true;
    }
    
    /**
     *
     * @return
     */
    @Override
    public ResultSet getInput() {
        return this.results;
    }

    /**
     * 
     * @param file
     * @throws XPortException 
     */
    @Override
    public void setOutput(File file) throws XPortException {
        this.output = file;
    }

    /**
     *
     * @return
     */
    @Override
    public File getOutput() {
        return this.output;
    }
    
    /**
     * 
     * @param result 
     */
    public void setResult(XPortResult result) {
        this.result = result;
    }

    /**
     *
     * @param max
     */
    public void setProgressMaximum(int max) {
        this.progress.setMaximum(max);
    }

    /**
     *
     * @param min
     */
    public void setProgressMinimum(int min) {
        this.progress.setMinimum(min);
    }

    /**
     *
     * @param value
     */
    public void setProgressValue(int value) {
        this.progress.setValue(value);
    }

    /**
     *
     * @return
     */
    public int getProgressValue() {
        return this.progress.getValue();
    }
    
    /**
     * 
     * @param evt 
     */
    private void btnExportActionPerformed(ActionEvent evt) {
        // Open the file chooser
        try {
            final JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Save File...");
            fileChooser.setCurrentDirectory(this.currentDirectory);
            fileChooser.setAcceptAllFileFilterUsed(true);
            fileChooser.setMultiSelectionEnabled(false);
            // Add a file filter
            for (FileFilter filter : getFileFilters()) {
                fileChooser.addChoosableFileFilter(filter);
            }
            // Show file chooser option is approved
            if (JFileChooser.APPROVE_OPTION == fileChooser.showSaveDialog(this)) {
                setOutput(fileChooser.getSelectedFile());
                try {
                    export();
                    if (this.chbOpenOnComplete.isSelected()) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Runtime.getRuntime().exec(fileChooser.getSelectedFile().getAbsolutePath());
                                } catch (IOException ioe) {
                                    setResult(new XPortResult(false, ioe.getMessage()));
                                }
                            }
                        }).start();
                    }
                } catch (XPortException xpe) {
                    setResult(new XPortResult(false, xpe.getMessage()));
                }
            }
        } catch (XPortException | HeadlessException e) {
            setResult(new XPortResult(false, e.getMessage()));
        } finally {
            setVisible(false);
            dispose();
        }
    }
    
    /**
     * 
     * @param evt 
     */
    private void progressStateChanged(ChangeEvent evt) {
        ((JProgressBar) evt.getSource()).update(((JProgressBar) evt.getSource()).getGraphics());
    }
    
    /**
     * This is where the work is done
     * @throws com.jm.xporta.exceptions.XPortException
     */
    public abstract void export() throws XPortException;
    
    /**
     * 
     * @return 
     */
    public abstract FileFilter[] getFileFilters();
    
    /**
     * 
     */
    @SuppressWarnings("unchecked")
    private void initComponents() {
        panelBackground = new ImagePanel();
        progress = new javax.swing.JProgressBar();
        btnExport = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        chbOpenOnComplete = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("XPorting...");
        setAlwaysOnTop(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                setVisible(false);
                dispose();
            }
        });

        progress.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                progressStateChanged(evt);
            }
        });

        btnExport.setText("Export");
        btnExport.setToolTipText("");
        btnExport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportActionPerformed(evt);
            }
        });

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setVisible(false);
                dispose();
            }
        });

        chbOpenOnComplete.setText("Open On Complete");
        chbOpenOnComplete.setOpaque(false);
        
        this.panelBackground.setImage(new File("./images/optionslogo.png"));
        javax.swing.GroupLayout panelBackgroundLayout = new javax.swing.GroupLayout(panelBackground);
        panelBackground.setLayout(panelBackgroundLayout);
        panelBackgroundLayout.setHorizontalGroup(
            panelBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBackgroundLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(progress, javax.swing.GroupLayout.DEFAULT_SIZE, 376, Short.MAX_VALUE)
                    .addGroup(panelBackgroundLayout.createSequentialGroup()
                        .addComponent(chbOpenOnComplete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCancel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnExport)))
                .addContainerGap())
        );
        panelBackgroundLayout.setVerticalGroup(
            panelBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBackgroundLayout.createSequentialGroup()
                .addGap(92, 92, 92)
                .addComponent(progress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnExport)
                    .addComponent(btnCancel)
                    .addComponent(chbOpenOnComplete))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelBackground, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelBackground, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }
    
}
