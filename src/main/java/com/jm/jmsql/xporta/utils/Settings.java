/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.xporta.utils;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author Michael L.R. Marques
 */
public class Settings {
    
    /**
     * The system file that will be used to load and save the Xml Document
     */
    private static File settingsFile = new File("./settings/settings.xml");
    
    /**
     * Gets the value of an attribute in a settings node
     * 
     * @param name
     * @return 
     */
    public static File getDatabasesFile() throws ParserConfigurationException, XPathExpressionException, SAXException, IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        return new File(XPathFactory.newInstance().newXPath().evaluate("/settings/databases/@value", factory.newDocumentBuilder().parse(settingsFile), XPathConstants.STRING).toString());
    }
    
}
