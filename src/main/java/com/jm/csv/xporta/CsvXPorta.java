/*
 * Copyright (C) 2013 Michael
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.jm.csv.xporta;

import com.jm.commons.fio.FileTypeFilter;
import com.jm.commons.utils.Constants;
import com.jm.jmsql.xporta.XPorta;
import com.jm.xporta.exceptions.XPortException;
import com.jm.xporta.plugin.XPort;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @created Apr 26, 2013
 * @author Michael L.R. Marques
 */
public class CsvXPorta extends XPorta implements XPort<ResultSet, File> {
    
    /**
     * 
     * @return 
     */
    @Override
    public String getName() {
        return "Csv XPorta";
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public String getDescription() {
        return "Exports database data to a *.csv file.";
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public boolean isExcluded() {
        return false;
    }
    
    /**
     * 
     * @return 
     */
    @Override public FileFilter[] getFileFilters() {
        return new FileFilter[] { new FileTypeFilter("csv", "CSV File") ,
                                    new FileTypeFilter("*", "All Files") };
    }
    
    /**
     * 
     * @throws XPortException 
     */
    @Override
    public void export() throws XPortException {
        // If data is null throw an exception
        if (getInput() == null) {
            throw new XPortException("XPort data was not initialized");
        }
        // If the output file does not exist, throw an exception
        if (getOutput() == null) {
            throw new XPortException("Output file was not initialized");
        }
        // Initialize needed variables
        ResultSet results = getInput();
        int totalColumns = 0;
        String[] columnNames;
        setProgressMinimum(0);
        setProgressValue(0);
        // Collect the result sets infromation
        try {
            results.last();
            setProgressMaximum(results.getRow());
            ResultSetMetaData meta = results.getMetaData();
            totalColumns = meta.getColumnCount();
            columnNames = new String[totalColumns];
            for (int i = 0; i < totalColumns; i++) {
                columnNames[i] = meta.getColumnName(i+1);
            }
            results.beforeFirst();
        } catch (SQLException sqle) {
            throw new XPortException(sqle.getMessage(), sqle);
        }
        // Catch IO errors and throw XPortException
        try {
            try (FileWriter writer = new FileWriter(getOutput())) {
                // Write the header
                for (String column : columnNames) {
                    writer.append(column);
                    writer.append(',');
                 }
                writer.append(Constants.NEW_LINE);
                // Start writting the data to the sheet
                try {
                    while(results.next()) {
                        for (int column = 1; column <= totalColumns; column++) {
                            Object object = results.getObject(column);
                            writer.append(object != null ? object.toString() : "");
                            writer.append(',');
                        }
                        writer.append(Constants.NEW_LINE);
                        setProgressValue(getProgressValue() + 1);
                    }
                } catch (SQLException sqle) {
                    throw new XPortException(sqle.getMessage(), sqle);
                }
            }
        } catch (IOException ioe) {
            throw new XPortException(ioe.getMessage(), ioe);
        }
    }

}
