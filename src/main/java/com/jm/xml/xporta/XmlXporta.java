/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.xml.xporta;

import com.jm.commons.components.JMDialog;
import com.jm.commons.fio.FileTypeFilter;
import com.jm.jmsql.xporta.XPorta;
import com.jm.xporta.exceptions.XPortException;
import com.jm.xporta.plugin.XPort;
import java.awt.Frame;
import java.io.File;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.swing.filechooser.FileFilter;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author marquesm
 */
public class XmlXporta extends XPorta implements XPort<ResultSet, File> {
    
    /**
     * 
     * @return 
     */
    @Override
    public String getName() {
        return "Xml Xporta";
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public String getDescription() {
        return "Exports database data to a *.xml file.";
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public boolean isExcluded() {
        return false;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public FileFilter[] getFileFilters() {
        return new FileFilter[] { new FileTypeFilter("xml", "XML File") };
    }
    
    /**
     * 
     * @throws com.jm.xporta.exceptions.XPortException
     */
    @Override
    public void export() throws XPortException {
        // If data is null throw an exception
        if (getInput() == null) {
            throw new XPortException("XPort data was not initialized");
        }
        // If the output file does not exist, throw an exception
        if (getOutput() == null) {
            throw new XPortException("Output file was not initialized");
        }
        // Initialize needed variables
        setProgressMinimum(0);
        setProgressValue(0);
        // Collect the result sets infromation
        try {
            ResultSet results = getInput();
            results.last();
            setProgressMaximum(results.getRow());
            ResultSetMetaData meta = results.getMetaData();
            int totalColumns = meta.getColumnCount();
            String[] columnNames = new String[totalColumns];
            for (int i = 0; i < totalColumns; i++) {
                columnNames[i] = meta.getColumnName(i+1);
            }
            results.beforeFirst();
            
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);
            Document xmlDocument = (Document) factory.newDocumentBuilder().newDocument();
            
            XmlNodes nodes = new XmlNodes(this);
            nodes.setIconImage(getIconImage());
            nodes.setVisible(true);
            String rootNodeName = nodes.getRootNodeName();
            String rowNodeName = nodes.getRowNodeName();
            
            Element root = xmlDocument.createElement(rootNodeName);
            xmlDocument.appendChild(root);
            
            while(results.next()) {
                Element row = xmlDocument.createElement(rowNodeName);
                for (String columnName : columnNames) {
                    Element column = xmlDocument.createElement(columnName);
                    column.setTextContent(String.valueOf(results.getObject(columnName)).trim());
                    row.appendChild(column);
                }
                root.appendChild(row);
                setProgressValue(getProgressValue() + 1);
            }
            TransformerFactory.newInstance().newTransformer().transform(new DOMSource(xmlDocument), new StreamResult(getOutput()));
        } catch (SQLException sqle) {
            throw new XPortException(sqle.getMessage(), sqle);
        } catch (TransformerConfigurationException | ParserConfigurationException e) {
            throw new XPortException(e.getMessage(), e);
        } catch (TransformerException te) {
            throw new XPortException(te.getMessage(), te);
        }
    }
    
    /**
     * 
     */
    private class XmlNodes extends JMDialog {
    
        private static final String ROW = "row";
        private static final String ROOT = "root";

        /**
         * Creates new form XmlNodes
         * @param parent
         */
        public XmlNodes(Frame parent) {
            super(parent, true);
            initComponents();
            this.pnlBackground.setImage(new File("./images/optionslogo.png"));
        }

        /**
         * 
         * @return 
         */
        public String getRootNodeName() {
            if (!this.txtRootNode.getText().isEmpty()) {
                return this.txtRootNode.getText();
            }
            return ROOT;
        }

        /**
         * 
         * @return 
         */
        public String getRowNodeName() {
            if (!this.txtRowNode.getText().isEmpty()) {
                return this.txtRowNode.getText();
            }
            return ROW;
        }
        
        /**
         * 
         */
        @SuppressWarnings("unchecked")                   
        private void initComponents() {

            pnlBackground = new com.jm.commons.components.panel.ImagePanel();
            txtRootNode = new com.jm.commons.components.textfield.HintJTextField();
            txtRowNode = new com.jm.commons.components.textfield.HintJTextField();
            btnDone = new javax.swing.JButton();

            setTitle("Setup Xml Nodes");
            setAlwaysOnTop(true);
            setResizable(false);

            txtRootNode.setHintText("Root Node Name");

            txtRowNode.setHintText("Row Node Name");

            btnDone.setText("Done");
            btnDone.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnDoneActionPerformed(evt);
                }
            });

            javax.swing.GroupLayout pnlBackgroundLayout = new javax.swing.GroupLayout(pnlBackground);
            pnlBackground.setLayout(pnlBackgroundLayout);
            pnlBackgroundLayout.setHorizontalGroup(
                pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlBackgroundLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txtRootNode, javax.swing.GroupLayout.DEFAULT_SIZE, 328, Short.MAX_VALUE)
                        .addComponent(txtRowNode, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBackgroundLayout.createSequentialGroup()
                            .addGap(0, 0, Short.MAX_VALUE)
                            .addComponent(btnDone)))
                    .addContainerGap())
            );
            pnlBackgroundLayout.setVerticalGroup(
                pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlBackgroundLayout.createSequentialGroup()
                    .addGap(90, 90, 90)
                    .addComponent(txtRootNode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(txtRowNode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnDone)
                    .addContainerGap())
            );

            javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
            getContentPane().setLayout(layout);
            layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(pnlBackground, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            );
            layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(pnlBackground, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            );

            pack();
        }

        private void btnDoneActionPerformed(java.awt.event.ActionEvent evt) {                                        
            setVisible(false);
        }                                       
        
        private javax.swing.JButton btnDone;
        private com.jm.commons.components.panel.ImagePanel pnlBackground;
        private com.jm.commons.components.textfield.HintJTextField txtRootNode;
        private com.jm.commons.components.textfield.HintJTextField txtRowNode;
        
    }
    
}
