/*
 * Copyright (C) 2013 Michael
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.jm.jdbc.xporta.objects;

//~--- JDK imports ------------------------------------------------------------

import java.io.File;

/**
 *
 * @author Michael L.R. Marques
 */
public class Database {
    
    //
    private String name;
    //
    private String driver;
    //
    private File   library;

    /**
     *
     * @param name
     * @param driver
     */
    public Database(String name, String driver) {
        this(name, driver, (File) null);
    }

    /**
     *
     * @param name
     * @param driver
     * @param library
     */
    public Database(String name, String driver, File library) {
        this.name    = name;
        this.driver  = driver;
        this.library = library;
    }

    /**
     *
     * @param name
     * @param driver
     * @param library
     */
    public Database(String name, String driver, String library) {
        this(name, driver, new File(library));
    }

    /**
     *
     * @return
     */
    public String getName() {
        return this.name;
    }

    /**
     *
     * @return
     */
    public String getDriver() {
        return this.driver;
    }

    /**
     *
     * @return
     */
    public File getLibrary() {
        return this.library;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return this.name;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
