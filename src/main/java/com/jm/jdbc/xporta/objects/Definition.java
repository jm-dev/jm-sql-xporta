/*
 * Copyright (C) 2013 Michael
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.jm.jdbc.xporta.objects;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.util.Properties;

/**
 *
 * @author Michael L.R. Marques
 */
public class Definition {
    
    //
    private Database database;
    //
    private String name;
    //
    private String datasource;
    //
    private String username;
    //
    private String password;
    
    /**
     * 
     * @param database
     * @param name
     * @param datasource
     * @param username
     * @param password 
     */
    public Definition(Database database, String name, String datasource, String username, String password) {
        this.database = database;
        this.name = name;
        this.datasource = datasource;
        this.username = username;
        this.password = password;
    }
    
    /**
     * 
     * @return 
     */
    public Database getDatabase() {
        return this.database;
    }
    
    /**
     * 
     * @return 
     */
    public String getName() {
        return this.name;
    }
    
    /**
     * 
     * @return 
     */
    public String getDatasource() {
        return this.datasource;
    }
    
    /**
     * 
     * @return 
     */
    public String getUsername() {
        return this.username;
    }
    
    /**
     * 
     * @return 
     */
    public String getPassword() {
        return this.password;
    }
    
    /**
     * 
     * @return
     * @throws IOException
     * @throws MalformedURLException
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SQLException 
     */
    public Connection connect() throws IOException, MalformedURLException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        try (URLClassLoader loader =  new URLClassLoader(new URL[] {new URL("jar:file:" + this.database.getLibrary().getAbsolutePath() + "!/")})) {
            Properties properties = new Properties();
            properties.put("user", this.username);
            properties.put("password", this.password);
            return ((Driver) loader.loadClass(this.database.getDriver()).newInstance()).connect(this.datasource, properties);
        }
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public String toString() {
        return this.name;
    }
    
}
