/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jxl.xporta.cell;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;
import jxl.write.Label;
import jxl.write.WritableCell;

/**
 *
 * @author Michael L.R. Marques
 */
public class JxlCellFactory {

    private static JxlCellFactory instance;

    /**
     *
     */
    private JxlCellFactory() {
        super();
    }

    /**
     * 
     * @return
     */
    public static JxlCellFactory getInstance() {
        if (instance == null) {
            instance = new JxlCellFactory();
        }
        return instance;
    }

    /**
     * 
     * @param column
     * @param row
     * @param type
     * @param results
     * @return
     * @throws SQLException
     */
    public WritableCell createCell(int column, int row, int type, ResultSet results) throws SQLException {
        switch (type) {
            case Types.NUMERIC:;
            case Types.DECIMAL: {
                BigDecimal decimal = results.getBigDecimal(column + 1);
                if (decimal == null) {
                    return new Label(column, row, "");
                }
                return new jxl.write.Number(column, row, decimal.doubleValue());
            } case Types.BIT: return new jxl.write.Boolean(column, row, Boolean.TRUE == results.getBoolean(column + 1));
            case Types.INTEGER: return new jxl.write.Number(column, row, results.getInt(column + 1));
            case Types.BIGINT: return new jxl.write.Number(column, row, results.getLong(column + 1));
            case Types.REAL:;
            case Types.FLOAT: return new jxl.write.Number(column, row, results.getFloat(column + 1));
            case Types.DOUBLE: return new jxl.write.Number(column, row, results.getDouble(column + 1));
            case Types.DATE:;
            case Types.TIME:;
            case Types.TIMESTAMP: {
                Date date =  results.getDate(column + 1);
                if (date == null) {
                    return new Label(column, row, "");
                }
                return new jxl.write.DateTime(column, row, date);
            } case Types.CHAR:;
            case Types.VARCHAR:;
            case Types.LONGNVARCHAR: return new Label(column, row, results.getString(column + 1));
            default: {
                Object object = results.getObject(column + 1);
                return new Label(column, row, object != null ? object.toString() : "");
            }
        }
    }

}
